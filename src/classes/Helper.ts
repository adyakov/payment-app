interface IBreadcrumb {
    title: string
    link: string
}

const dictionary =
    'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum'

class Helper {
    public static getBreadcrumbs(url: string): IBreadcrumb[] {
        const result = [
            {
                title: 'Home',
                link: '/',
            },
        ]
        const urlArr = url.split('/')
        urlArr.forEach(
            (item, index) =>
                item.length &&
                result.push({
                    title:
                        ['cards', 'transactions'].indexOf(item) + 1
                            ? `/${item.charAt(0).toUpperCase()}${item.slice(1)}`
                            : `/${item}`,
                    link:
                        index < urlArr.length - 1
                            ? `${result.slice(-1)[0].link}${item}/`
                            : '',
                })
        )

        return result
    }

    public static getRandomIntInInterval(min: number, max: number): number {
        return Math.round(min - 0.5 + Math.random() * (max - min + 1))
    }

    public static getRandomString(length: number): string {
        const dictionaryArr = dictionary.split(' ')
        const entry = this.getRandomIntInInterval(
            0,
            dictionaryArr.length - length - 1
        )
        return dictionaryArr.slice(entry, entry + length).join(' ')
    }
}

export default Helper
