import { combineReducers, configureStore } from '@reduxjs/toolkit'
import userReducer from './reducers/auth'
import cardReducer from './reducers/card'
import transactionReducer from './reducers/transaction'
import cardFilterReducer from './reducers/cardFilter'
import transactionFilterReducer from './reducers/transactionFilter'

const rootReducer = combineReducers({
    user: userReducer,
    card: cardReducer,
    transaction: transactionReducer,
    cardFilter: cardFilterReducer,
    transactionFilter: transactionFilterReducer,
})

export const store = configureStore({
    reducer: rootReducer,
})
