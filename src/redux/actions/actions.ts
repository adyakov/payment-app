import {
    LOGIN,
    SET_CARDS,
    SET_TRANSACTIONS,
    SET_CARD_FILTER,
    SET_TRANSACTIONS_FILTER,
} from '../types'
import { ICard, ITransaction, IUser } from '../../types/dataTypes'

export function login(user: IUser) {
    return {
        type: LOGIN,
        payload: user,
    }
}

export function setCards(cards: ICard[]) {
    return {
        type: SET_CARDS,
        payload: cards,
    }
}

export function setTransactions(transactions: ITransaction[]) {
    return {
        type: SET_TRANSACTIONS,
        payload: transactions,
    }
}

export function setCardsFilter(cardFilter: any) {
    return {
        type: SET_CARD_FILTER,
        payload: cardFilter,
    }
}

export function setTransactionsFilter(transactionFilter: any) {
    return {
        type: SET_TRANSACTIONS_FILTER,
        payload: transactionFilter,
    }
}
