import {
    IInitialCardsFilterState,
    IActionCardFilter,
} from '../../types/dataTypes'
import { SET_CARD_FILTER } from '../types'

const initialCardState: IInitialCardsFilterState = {
    cardID: [],
    cardAccount: [],
    currency: [],
    status: [],
}

function cardFilterReducer(
    state = initialCardState,
    action: IActionCardFilter
) {
    switch (action.type) {
        case SET_CARD_FILTER:
            return { ...state, ...action.payload }
        default:
            return state
    }
}

export default cardFilterReducer
