import {
    IInitialTransactionsFilterState,
    IActionTransactionFilter,
} from '../../types/dataTypes'
import { SET_TRANSACTIONS_FILTER } from '../types'

const initialFilterState: IInitialTransactionsFilterState = {
    cardID: [],
    cardAccount: [],
    amount: [],
    currency: [],
    date: [],
}

function transactionFilterReducer(
    state = initialFilterState,
    action: IActionTransactionFilter
) {
    switch (action.type) {
        case SET_TRANSACTIONS_FILTER:
            return { ...state, ...action.payload }
        default:
            return state
    }
}

export default transactionFilterReducer
