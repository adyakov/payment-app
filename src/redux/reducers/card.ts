import { IInitialCardState, IActionCard } from '../../types/dataTypes'
import { SET_CARDS } from '../types'

const initialCardState: IInitialCardState = {
    cards: [],
}

function cardReducer(state = initialCardState, action: IActionCard) {
    switch (action.type) {
        case SET_CARDS:
            return { ...state, cards: action.payload }
        default:
            return state
    }
}

export default cardReducer
