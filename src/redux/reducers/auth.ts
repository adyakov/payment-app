import { IInitialUserState, IActionUser } from '../../types/dataTypes'
import { LOGIN } from '../types'

const initialUserState: IInitialUserState = {
    user: null,
    isLoggedIn: false,
}

function userReducer(state = initialUserState, action: IActionUser) {
    switch (action.type) {
        case LOGIN:
            return { ...state, user: action.payload, isLoggedIn: true }
        default:
            return state
    }
}

export default userReducer
