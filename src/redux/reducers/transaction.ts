import {
    IInitialTransactionsState,
    IActionTransaction,
} from '../../types/dataTypes'
import { SET_TRANSACTIONS } from '../types'

const initialTransactionState: IInitialTransactionsState = {
    transactions: [],
}

function transactionReducer(
    state = initialTransactionState,
    action: IActionTransaction
) {
    switch (action.type) {
        case SET_TRANSACTIONS:
            return { ...state, transactions: action.payload }
        default:
            return state
    }
}

export default transactionReducer
