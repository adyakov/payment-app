export interface ICard {
    cardID: string
    cardAccount: number
    maskedCardNumber: number
    expireDate: string
    currency: string
    status: string
    balance: string
}

export interface ITransaction {
    transactionID: string
    cardAccount: number
    cardID: string
    amount: string
    currency: string
    transactionDate: string
    merchantInfo: string
}

export interface IUser {
    id: string
    login: string
    name: string
}

export interface IRootState {
    user: {
        isLoggedIn: boolean
        user: IUser | null
    }
    transaction: IInitialTransactionsState
    card: IInitialCardState
    transactionFilter: IInitialTransactionsFilterState
    cardFilter: IInitialCardsFilterState
}

export interface IInitialUserState {
    user: IUser | null
    isLoggedIn: boolean
}

export interface IInitialTransactionsState {
    transactions: ITransaction[]
}

export interface IInitialCardState {
    cards: ICard[]
}

export interface IInitialTransactionsFilterState {
    cardID: string[]
    cardAccount: string[]
    amount: string[]
    currency: string[]
    date: string[]
}

export interface IInitialCardsFilterState {
    cardID: string[]
    cardAccount: string[]
    currency: string[]
    status: string[]
}

export interface IAction {
    payload: any
    type: string
}

export interface IActionUser extends IAction {
    payload: IUser
}

export interface IActionTransaction extends IAction {
    payload: ITransaction[]
}

export interface IActionCard extends IAction {
    payload: ICard[]
}

export interface IActionTransactionFilter extends IAction {
    payload: any
}

export interface IActionCardFilter extends IAction {
    payload: any
}
