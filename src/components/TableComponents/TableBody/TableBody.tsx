import React from 'react'
import cl from './TableBody.module.css'

interface columnConfig {
    title: string
    key: string
    render?: (el: any, key: number) => any
}

interface ITableProps {
    data: any[]
    columns: columnConfig[]
}

const TableBody = ({ data, columns }: ITableProps) => {
    return (
        <div className={cl.table}>
            <table cellPadding={0} cellSpacing={0}>
                <thead>
                    <tr>
                        {columns.map((column) => (
                            <th key={column.key}>{column.title}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {data.map((row, index) => (
                        <tr
                            className={index % 2 === 0 ? cl.darkRow : ''}
                            key={index}
                        >
                            {columns.map((column) => (
                                <td key={column.key}>
                                    {column.render
                                        ? column.render(row, index)
                                        : row[column.key]}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default TableBody
