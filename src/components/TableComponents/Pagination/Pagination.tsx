import React from 'react'
import cl from './Pagination.module.css'

interface IPaginationProps {
    currentPage: number
    pages: number
    setPageHandle: (page: number) => any
}

const Pagination = ({
    currentPage,
    pages,
    setPageHandle,
}: IPaginationProps) => {
    const getPages = () => {
        const pagesArr = []
        for (let i = 1; i <= pages; i++) {
            pagesArr.push(
                <div
                    key={i}
                    onClick={() => setPageHandle(i)}
                    className={
                        currentPage === i
                            ? `${cl.paginationButtonSelected} ${cl.paginationButton}`
                            : cl.paginationButton
                    }
                >
                    {i}
                </div>
            )
        }
        return pagesArr
    }

    return <div className={cl.pagination}>{getPages()}</div>
}

export default Pagination
