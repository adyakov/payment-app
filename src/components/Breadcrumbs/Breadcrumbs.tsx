import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import cl from './Breadcrumbs.module.css'
import Helper from '../../classes/Helper'

const Breadcrumbs = () => {
    const location = useLocation()
    const breadcrumbs = Helper.getBreadcrumbs(location.pathname)

    return (
        <div className={cl.breadcrumbs}>
            {breadcrumbs.map((item) => (
                <div key={item.title}>
                    {item.link.length ? (
                        <Link to={item.link}>{item.title}</Link>
                    ) : (
                        <div>{item.title}</div>
                    )}
                </div>
            ))}
        </div>
    )
}

export default Breadcrumbs
