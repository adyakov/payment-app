import React from 'react'
import cl from './Card.module.css'

interface IBlockConfig {
    title: string
    key: string
    render?: (el: any) => any
}

interface ICardProps {
    blocks: IBlockConfig[]
    item: any
}

const Card = ({ item, blocks }: ICardProps) => {
    return (
        <div className={cl.card}>
            {blocks.map((row) => (
                <div key={row.key} className={cl.cardRow}>
                    <div className={cl.cardRowTitle}>{row.title}:</div>
                    <div>{row.render ? row.render(item) : item[row.key]}</div>
                </div>
            ))}
        </div>
    )
}

export default Card
