import React from 'react'
import cl from './Filter.module.css'

interface IFilterConfig {
    title: string
    type: string
    key: string
    values: Map<any, any>
}

interface IFilterProps {
    fields: any
    selectedItems: any
    filterHandle: (key: string, val: any) => any
}
const Filter = ({ fields, filterHandle, selectedItems }: IFilterProps) => {
    const getFilter = (filterConfig: IFilterConfig) => {
        switch (filterConfig.type) {
            case 'select':
            case 'selectMultiple':
                return (
                    <div>
                        <select
                            onChange={(e) =>
                                filterHandle(
                                    filterConfig.key,
                                    Object.values(e.target.options)
                                        .filter((option) => option.selected)
                                        .map((item) => item.label)
                                )
                            }
                            multiple={filterConfig.type === 'selectMultiple'}
                        >
                            {Array.from(filterConfig.values.values()).map(
                                (item) => (
                                    <option
                                        selected={
                                            selectedItems[
                                                filterConfig.key
                                            ].indexOf(item) + 1
                                        }
                                        value={item}
                                        key={item}
                                    >
                                        {item}
                                    </option>
                                )
                            )}
                        </select>
                    </div>
                )
            default:
                return <div></div>
        }
    }

    return (
        <div className={cl.filter}>
            {Object.entries(fields).map((entry: any[]) => (
                <div>
                    <div className={cl.filterLabel}>{entry[1].title}</div>
                    <div>{getFilter(entry[1])}</div>
                </div>
            ))}
        </div>
    )
}

export default Filter
