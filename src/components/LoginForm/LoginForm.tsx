import React from 'react'

interface ILoginFormProps {
    loginHandle: (event: any) => Promise<void>
}

const LoginForm = ({ loginHandle }: ILoginFormProps) => {
    return (
        <div>
            <form onSubmit={loginHandle}>
                <input placeholder={'password'} />
                <button type={'submit'}>Login</button>
            </form>
        </div>
    )
}

export default LoginForm
