import React, { useState } from 'react'
import Pagination from '../TableComponents/Pagination/Pagination'
import TableBody from '../TableComponents/TableBody/TableBody'

interface IColumnConfig {
    title: string
    key: string
    render?: (el: any, key: number) => any
}

interface ITableProps {
    data: any[]
    columns: IColumnConfig[]
    itemsPerPage?: number
}

const Table = ({ data, columns, itemsPerPage }: ITableProps) => {
    const [currentPage, setCurrentPage] = useState(1)
    const setPageHandle = (page: number) => {
        setCurrentPage(page)
    }

    return (
        <div>
            <TableBody
                data={
                    itemsPerPage
                        ? data.slice(
                              itemsPerPage * currentPage - itemsPerPage,
                              itemsPerPage * currentPage
                          )
                        : data
                }
                columns={columns}
            />
            {itemsPerPage && itemsPerPage < data.length && (
                <Pagination
                    currentPage={currentPage}
                    pages={Math.ceil(data.length / itemsPerPage)}
                    setPageHandle={setPageHandle}
                />
            )}
        </div>
    )
}

export default Table
