import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import cl from './NavBar.module.css'

const NavBar = () => {
    const location = useLocation()
    const rootSection = location.pathname.split('/')[1]

    return (
        <div className={cl.menu}>
            <Link to={'/transactions'}>
                <div
                    className={
                        rootSection === 'transactions' ? cl.selected : ''
                    }
                >
                    Transactions
                </div>
            </Link>
            <Link to={'/cards'}>
                <div className={rootSection === 'cards' ? cl.selected : ''}>
                    Cards
                </div>
            </Link>
        </div>
    )
}

export default NavBar
