import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import { publicRoutes, protectedRoutes } from '../../routes'
import { useSelector } from 'react-redux'
import { IRootState } from '../../types/dataTypes'

const AppRouter = () => {
    const { isLoggedIn } = useSelector((state: IRootState) => state.user)

    return (
        <Routes>
            {isLoggedIn
                ? protectedRoutes.map((route) => {
                      return (
                          <Route
                              path={route.path}
                              element={route.element}
                              key={Date.now()}
                          />
                      )
                  })
                : publicRoutes.map((route) => {
                      return (
                          <Route
                              path={route.path}
                              element={route.element}
                              key={Date.now()}
                          />
                      )
                  })}
            <Route path="*" element={<Navigate to="/" />} />
        </Routes>
    )
}

export default AppRouter
