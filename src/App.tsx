import React, { Suspense } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import './App.css'
import AppRouter from './components/AppRouter/AppRouter'

function App() {
    return (
        <div className="App">
            <Suspense fallback={<div className={'loader'}>Loading...</div>}>
                <Router>
                    <AppRouter />
                </Router>
            </Suspense>
        </div>
    )
}

export default App
