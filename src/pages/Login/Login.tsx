import React from 'react'
import { useDispatch } from 'react-redux'
import { login, setCards, setTransactions } from '../../redux/actions/actions'
import LoginForm from '../../components/LoginForm/LoginForm'
import AuthAPIService from '../../API/authAPI'
import CardAPIService from '../../API/cardAPI'
import TransactionAPIService from '../../API/transactionAPI'

const Login = () => {
    const dispatch = useDispatch()

    const loginHandle = async (event: any): Promise<void> => {
        event.preventDefault()
        const userData = await AuthAPIService.login()
        const cardData = await CardAPIService.getCards()
        const transactionData = await TransactionAPIService.getTransactions()
        dispatch(login(userData))
        dispatch(setCards(cardData))
        dispatch(setTransactions(transactionData))
    }

    return (
        <div className={'loginPage'}>
            <LoginForm loginHandle={loginHandle} />
        </div>
    )
}

export default Login
