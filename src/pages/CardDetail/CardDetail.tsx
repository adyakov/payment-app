import React from 'react'
import NavBar from '../../components/NavBar/NavBar'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs'
import Card from '../../components/Card/Card'
import { Link, useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { ICard, IRootState } from '../../types/dataTypes'

const CardDetail = () => {
    const { cardID, transactionID } = useParams()
    const cards = useSelector((state: IRootState) => state.card.cards)

    const cardBlocks = [
        {
            title: 'Card ID',
            key: 'cardID',
        },
        {
            title: 'Card Account',
            key: 'cardAccount',
        },
        {
            title: 'Masked Card Number',
            key: 'maskedCardNumber',
        },
        {
            title: 'Expire Date',
            key: 'expireDate',
            render: (el: ICard) => (
                <div>{new Date(el.expireDate).toLocaleDateString()}</div>
            ),
        },
        {
            title: 'Currency',
            key: 'currency',
        },
        {
            title: 'Status',
            key: 'status',
        },
        {
            title: 'Balance',
            key: 'balance',
        },
    ]

    if (!(transactionID && cardID)) {
        cardBlocks.push({
            title: 'Transactions',
            key: 'transactions',
            render: (el: ICard) => <Link to={'transactions'}>Show</Link>,
        })
    }

    return (
        <div className={'pageComponent'}>
            <NavBar />
            <div className={'pageContent'}>
                <Breadcrumbs />
                <Card
                    blocks={cardBlocks}
                    item={cards.filter((card) => card.cardID === cardID)[0]}
                />
            </div>
        </div>
    )
}

export default CardDetail
