import React from 'react'
import NavBar from '../../components/NavBar/NavBar'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs'
import Table from '../../components/Table/Table'
import Filter from '../../components/Filter/Filter'
import { useDispatch, useSelector } from 'react-redux'
import { IRootState, ITransaction } from '../../types/dataTypes'
import { Link, useParams } from 'react-router-dom'
import { setTransactionsFilter } from '../../redux/actions/actions'

const TransactionList = () => {
    const dispatch = useDispatch()
    const { cardID } = useParams()
    const transactions = useSelector(
        (state: IRootState) => state.transaction.transactions
    )
    const transactionFilter = useSelector(
        (state: IRootState | any) => state.transactionFilter
    )

    const data = cardID
        ? transactions.filter((transaction) => transaction.cardID === cardID)
        : transactions

    const columns = [
        {
            title: 'Transaction ID',
            key: 'transactionID',
        },
        {
            title: 'Card Account',
            key: 'cardAccount',
        },
        {
            title: 'Card ID',
            key: 'cardID',
        },
        {
            title: 'Amount',
            key: 'amount',
        },
        {
            title: 'Currency',
            key: 'currency',
        },
        {
            title: 'Transaction Date',
            key: 'transactionDate',
            render: (el: ITransaction, key: number) => (
                <div>{new Date(el.transactionDate).toLocaleString()}</div>
            ),
        },
        {
            title: 'Merchant Info',
            key: 'merchantInfo',
        },
        {
            title: 'Details',
            key: 'details',
            render: (el: ITransaction, key: number) => (
                <Link to={el.transactionID}>Transaction detail</Link>
            ),
        },
    ]

    const prepareFilters = () => {
        const filterFields = {
            cardID: {
                title: 'Card ID',
                key: 'cardID',
                type: 'selectMultiple',
                values: new Map(),
            },
            cardAccount: {
                title: 'Card Account',
                key: 'cardAccount',
                type: 'selectMultiple',
                values: new Map(),
            },
            amount: {
                title: 'Amount',
                key: 'amount',
                type: 'select',
                values: new Map(),
            },
            currency: {
                title: 'Currency',
                key: 'currency',
                type: 'selectMultiple',
                values: new Map(),
            },
            date: {
                title: 'Date',
                key: 'date',
                type: 'select',
                values: new Map(),
            },
        }

        const rawData = cardID ? data : filterData()
        rawData.forEach((item) => {
            filterFields.cardID.values.set(item.cardID, item.cardID)
            filterFields.cardAccount.values.set(
                item.cardAccount,
                item.cardAccount
            )
            filterFields.amount.values.set(item.amount, item.amount)
            filterFields.currency.values.set(item.currency, item.currency)
            filterFields.date.values.set(
                new Date(item.transactionDate).toLocaleDateString(),
                new Date(item.transactionDate).toLocaleDateString()
            )
        })

        return filterFields
    }

    const filterHandle = (key: string, val: any) => {
        const tempTransactionFilter = { ...transactionFilter }
        tempTransactionFilter[key] = val
        dispatch(setTransactionsFilter(tempTransactionFilter))
    }

    const filterData = () =>
        data.filter((item) => {
            if (transactionFilter.amount.length) {
                if (!(transactionFilter.amount.indexOf(item.amount) + 1)) {
                    return false
                }
            }
            if (transactionFilter.cardAccount.length) {
                if (
                    !(
                        transactionFilter.cardAccount.indexOf(
                            String(item.cardAccount)
                        ) + 1
                    )
                ) {
                    return false
                }
            }
            if (transactionFilter.cardID.length) {
                if (
                    !(transactionFilter.cardID.indexOf(String(item.cardID)) + 1)
                ) {
                    return false
                }
            }
            if (transactionFilter.currency.length) {
                if (
                    !(
                        transactionFilter.currency.indexOf(
                            String(item.currency)
                        ) + 1
                    )
                ) {
                    return false
                }
            }
            if (transactionFilter.date.length) {
                if (
                    !(
                        transactionFilter.date.indexOf(
                            String(
                                new Date(
                                    item.transactionDate
                                ).toLocaleDateString()
                            )
                        ) + 1
                    )
                ) {
                    return false
                }
            }
            return true
        })

    return (
        <div className={'pageComponent'}>
            <NavBar />
            <div className={'pageContent'}>
                <Breadcrumbs />
                {!cardID && (
                    <Filter
                        selectedItems={transactionFilter}
                        fields={prepareFilters()}
                        filterHandle={filterHandle}
                    />
                )}
                <Table
                    columns={columns}
                    data={cardID ? data : filterData()}
                    itemsPerPage={10}
                />
            </div>
        </div>
    )
}

export default TransactionList
