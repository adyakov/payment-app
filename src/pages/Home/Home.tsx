import React from 'react'
import NavBar from '../../components/NavBar/NavBar'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs'

const Home = () => {
    return (
        <div className={'pageComponent'}>
            <NavBar />
            <div className={'pageContent'}>
                <Breadcrumbs />
                <div>Home sweet home</div>
            </div>
        </div>
    )
}

export default Home
