import React from 'react'
import NavBar from '../../components/NavBar/NavBar'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs'
import Table from '../../components/Table/Table'
import Filter from '../../components/Filter/Filter'
import { useDispatch, useSelector } from 'react-redux'
import { ICard, IRootState } from '../../types/dataTypes'
import { Link } from 'react-router-dom'
import { setCardsFilter } from '../../redux/actions/actions'

const CardList = () => {
    const dispatch = useDispatch()
    const cards = useSelector((state: IRootState) => state.card.cards)
    const cardFilter = useSelector(
        (state: IRootState | any) => state.cardFilter
    )
    const columns = [
        {
            title: 'Card ID',
            key: 'cardID',
        },
        {
            title: 'Card Account',
            key: 'cardAccount',
        },
        {
            title: 'Masked Card Number',
            key: 'maskedCardNumber',
        },
        {
            title: 'Expire Date',
            key: 'expireDate',
            render: (el: ICard, key: number) => (
                <div>{new Date(el.expireDate).toLocaleDateString()}</div>
            ),
        },
        {
            title: 'Currency',
            key: 'currency',
        },
        {
            title: 'Status',
            key: 'status',
        },
        {
            title: 'Balance',
            key: 'balance',
        },
        {
            title: 'Details',
            key: 'details',
            render: (el: ICard, key: number) => (
                <Link to={el.cardID}>Card detail</Link>
            ),
        },
    ]

    const prepareFilters = () => {
        const filterFields = {
            cardID: {
                title: 'Card ID',
                key: 'cardID',
                type: 'selectMultiple',
                values: new Map(),
            },
            cardAccount: {
                title: 'Card Account',
                key: 'cardAccount',
                type: 'selectMultiple',
                values: new Map(),
            },
            currency: {
                title: 'Currency',
                key: 'currency',
                type: 'selectMultiple',
                values: new Map(),
            },
            status: {
                title: 'Status',
                key: 'status',
                type: 'select',
                values: new Map(),
            },
        }

        const rawData = filterData()
        rawData.forEach((item) => {
            filterFields.cardID.values.set(item.cardID, item.cardID)
            filterFields.cardAccount.values.set(
                item.cardAccount,
                item.cardAccount
            )
            filterFields.currency.values.set(item.currency, item.currency)
            filterFields.status.values.set(item.status, item.status)
        })

        return filterFields
    }

    const filterHandle = (key: string, val: any) => {
        const tempCardFilter = { ...cardFilter }
        tempCardFilter[key] = val
        dispatch(setCardsFilter(tempCardFilter))
    }

    const filterData = () =>
        cards.filter((item) => {
            if (cardFilter.cardAccount.length) {
                if (
                    !(
                        cardFilter.cardAccount.indexOf(
                            String(item.cardAccount)
                        ) + 1
                    )
                ) {
                    return false
                }
            }
            if (cardFilter.cardID.length) {
                if (!(cardFilter.cardID.indexOf(String(item.cardID)) + 1)) {
                    return false
                }
            }
            if (cardFilter.currency.length) {
                if (!(cardFilter.currency.indexOf(String(item.currency)) + 1)) {
                    return false
                }
            }
            if (cardFilter.status.length) {
                if (!(cardFilter.status.indexOf(String(item.status)) + 1)) {
                    return false
                }
            }
            return true
        })

    return (
        <div className={'pageComponent'}>
            <NavBar />
            <div className={'pageContent'}>
                <Breadcrumbs />
                <Filter
                    selectedItems={cardFilter}
                    fields={prepareFilters()}
                    filterHandle={filterHandle}
                />
                <Table
                    columns={columns}
                    data={filterData()}
                    itemsPerPage={10}
                />
            </div>
        </div>
    )
}

export default CardList
