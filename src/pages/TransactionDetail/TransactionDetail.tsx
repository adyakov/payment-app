import React from 'react'
import NavBar from '../../components/NavBar/NavBar'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs'
import Card from '../../components/Card/Card'
import { useParams } from 'react-router'
import { useSelector } from 'react-redux'
import { IRootState, ITransaction } from '../../types/dataTypes'
import { Link } from 'react-router-dom'

const TransactionDetail = () => {
    const { cardID, transactionID } = useParams()
    const transactions = useSelector(
        (state: IRootState) => state.transaction.transactions
    )

    const transactionBlocks = [
        {
            title: 'Transaction ID',
            key: 'transactionID',
        },
        {
            title: 'Card Account',
            key: 'cardAccount',
        },
        {
            title: 'Card ID',
            key: 'cardID',
        },
        {
            title: 'Amount',
            key: 'amount',
        },
        {
            title: 'Currency',
            key: 'currency',
        },
        {
            title: 'Transaction Date',
            key: 'transactionDate',
            render: (el: ITransaction) => (
                <div>{new Date(el.transactionDate).toLocaleString()}</div>
            ),
        },
        {
            title: 'Merchant Info',
            key: 'merchantInfo',
        },
    ]

    if (!(cardID && transactionID)) {
        transactionBlocks.push({
            title: 'Card detail',
            key: 'card',
            render: (el: ITransaction) => <Link to={el.cardID}>show</Link>,
        })
    }

    return (
        <div className={'pageComponent'}>
            <NavBar />
            <div className={'pageContent'}>
                <Breadcrumbs />
                <Card
                    blocks={transactionBlocks}
                    item={
                        transactions.filter(
                            (transaction) =>
                                transaction.transactionID === transactionID
                        )[0]
                    }
                />
            </div>
        </div>
    )
}

export default TransactionDetail
